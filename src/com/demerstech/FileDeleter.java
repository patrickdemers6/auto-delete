package com.demerstech;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Calendar;

public class FileDeleter {
    public static void main(String[] args) {
        // write your code here
        String path = "";
        int days = 7;
        for (String s : args) {
            String[] values = s.split("=");
            switch (values[0]) {
                case "--path":
                    if (values[1].endsWith("/") || values[1].endsWith("\\\\")) {
                        path = values[1];
                    }
                    break;
                case "--days":
                    try {
                        days = Integer.parseInt(values[1]);
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid date passed. Using default of " + days + " days.");
                    }
                    if (days < 0)
                        days = 7;
                    break;
                case "--help":
                    System.out.println(
                            "This program will automatically delete all files in a folder that have not been accessed for a specified amount of time.");
                    System.out.println(
                            ANSI_RED + "*** WARNING - THIS PROGRAM WILL PERMANENTLY DELETE FILES ***\n" + ANSI_RESET);
                    System.out.println("Valid flags:");
                    System.out.println(
                            "--path=[absolute path]      Specifies the path to the folder to delete. Must end in '\\\\' or '/'");
                    System.out.println(
                            "--days=[integer]            Specifies the minimum number of days for a file to not have been accessed before deletion.");
                    System.exit(0);
                    break;
            }
        }
        if (path.equals("")) {
            invalidPath("");
        }
        File[] filesInDir;
        try {
            filesInDir = getFiles(path);
            processFiles(filesInDir, days);
        } catch (NullPointerException e) {
            invalidPath(e.getMessage());
        }
    }

    private static void invalidPath(String msg) {
        System.out.println("Invalid path passed. Use the --help flag for help.");
        System.out.println(msg);
        System.exit(1);
    }

    private static void processFiles(File[] filesToProcess, int maximumDaysOld) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -maximumDaysOld);
        long ageToDelete = calendar.getTimeInMillis();
        for (File file : filesToProcess) {
            if (!checkPermissions(file, false)) {
                // skip if not valid permissions
                System.out.println("Not processing " + file.getName() + ". See errors above.\n");
                break;
            }
            try {
                Path filePath = file.toPath();
                BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
                long lastAccessed = attributes.lastAccessTime().toMillis();
                if (lastAccessed < ageToDelete) {
                    boolean deleteSuccess = file.delete();
                    if (!deleteSuccess)
                        System.out.println("Failed to delete " + file.getName());
                }
            } catch (IOException e) {
                System.out.println("Deleting " + file.getName() + " failed. Error:");
                System.out.println(e.toString());
            }
        }

    }

    private static File[] getFiles(String dirPath) {
        File directory = new File(dirPath);
        boolean validPath = checkPermissions(directory, true);
        if (!validPath) {
            System.out.println("Stopping execution. Modify directory to fix errors listed above.");
            System.exit(1);
        }
        return directory.listFiles();
    }

    private static boolean checkPermissions(File file, boolean directory) {
        boolean isValid = true;
        String name;
        if (directory) {
            name = file.getPath();
        }
        else if (file.isDirectory()) {
            System.out.println("Directory found, expected file. Skipping. " + file.getPath());
            return false;
        } else {
            name = file.getName();
        }
        try {
            if (!file.canWrite()) {
                System.out.println("Cannot write to " + name + ".");
                isValid = false;
            }
            if (!file.canRead()) {
                System.out.println("Cannot read " + name + ".");
                isValid = false;
            }
            if (!file.exists()) {
                System.out.println(name + " does not exist.");
                isValid = false;
            }
            if (directory && !file.isDirectory()) {
                System.out.println("Path not passed.");
                isValid = false;
            }
        } catch (SecurityException e) {
            System.out.println("Security exception on " + file.getName());
            isValid = false;
        } finally {
            return isValid;
        }
    }

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
}
