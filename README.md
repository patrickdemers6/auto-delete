# Auto Delete

## Introduction

This script automatically deletes files in the specified folder that have not been accessed in x number of days. It has been tested to work on Linux and Windows. Files are not moved to the trash (or your operating system's equivalent) as they are deleted permanently.

Great to keep your downloads folder tidy!

## Usage
`java -jar autodelete.jar --path=[absolute path to folder] --days=[days without edit]`
### Arguments
Path: The path to the folder containing files to auto delete. Must end in `/` or `\\`.

Days: The number of days old a file's last accessed date can be before being automatically deleted. Defaults to 7.

### Cron Job Example

Run at the top of every hour:

`0 * * * * java -jar <path to file>/autodelete.jar --path=<path to folder>/ --days=5`